package me.yiyu.leetcode.week3;

import java.util.Arrays;
import java.util.Comparator;
import me.yiyu.leetcode.random.ListNode;

public class LC52NQueensII {
  static class Solution {
    public int totalNQueens(int n) {
      int[] res = {0};
      int[] col = new int[n];
      Arrays.fill(col, -1);
      dfs(n, 0, col, res);
      return res[0];
    }

    private void dfs(int n, int row, int[] col, int[] res) {
      if (row == n) {
        res[0] += 1;
        return;
      }
      for (int i = 0; i < n; i++) {
        col[row] = i;  // (row, col[row]) => (row, i)
        if ( isValid(row, col) ) {
          dfs(n, row+1, col, res);
        }
        // col[row] = -1;
      }
    }

    private boolean isValid(int row, int[] col) {
      for (int i = 0; i < row; i++) {
        if (col[row] == col[i]
            || Math.abs(col[row] - col[i]) == row - i) {
          return false;
        }
      }
      return true;
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.totalNQueens(4));
  }
}
