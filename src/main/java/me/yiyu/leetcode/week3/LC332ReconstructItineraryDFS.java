package me.yiyu.leetcode.week3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LC332ReconstructItineraryDFS {
  static class Solution {
    public List<String> findItinerary(String[][] tickets) {
      Map<String, List<String>> flights = new HashMap<>();
      List<String> path = new ArrayList<>();
      int totalLen = tickets.length + 1;

      // construct the graph
      for (String[] ticket: tickets) {
        flights.computeIfAbsent(ticket[0], k -> new ArrayList<>())
            .add(ticket[1]);
      }

      // get the destination sorted
      for (List<String> list : flights.values()) {
        Collections.sort(list);
      }

      path.add("JFK");
      if (dfs(flights, totalLen, "JFK", path)) {
        return path;
      }

      return null;
    }

    private boolean dfs(Map<String, List<String>> flights,
        int totalLen,
        String departure,
        List<String> path) {
      if (path.size() == totalLen) {
        return true;
      }

      if (!flights.containsKey(departure)) {
        return false;
      }

      List<String> destinations = flights.get(departure);
      for (int i = 0; i < destinations.size(); i++) {
        String dest = destinations.get(i);
        destinations.remove(i);
        path.add(dest);
        if (dfs(flights, totalLen, dest, path)) {
          return true;
        }
        path.remove(path.size() - 1);
        destinations.add(i, dest);
      }

      return false;
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    String[][] tickets = {
        {"JFK", "SFO"},
        {"JFK", "ATL"},
        {"SFO", "ATL"},
        {"ATL", "JFK"},
        {"ATL", "SFO"},
    };
    System.out.println(s.findItinerary(tickets));
  }

}
