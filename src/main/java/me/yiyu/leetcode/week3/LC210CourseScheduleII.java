package me.yiyu.leetcode.week3;

import java.util.*;

public class LC210CourseScheduleII {

  static public class Solution {

    public int[] findOrder(int numCourses, int[][] prerequisites) {

      Map<Integer, Set<Integer>> graph = new HashMap<>();
      for (int i = 0; i < prerequisites.length; i++) {
        graph.computeIfAbsent(prerequisites[i][1], k -> new HashSet<>())
            .add(prerequisites[i][0]);
      }

      // 0: unknown; 1: visiting; 2: visited
      int[] visiting = new int[numCourses];
      Deque<Integer> stack = new LinkedList<>();
      for (int i = 0; i < numCourses; i++) {
        if (!topologicalSort(graph, i, stack, visiting)) {
          return new int[0];
        }
      }

      return stack.stream().mapToInt(i->i).toArray();
    }

    private boolean topologicalSort(Map<Integer, Set<Integer>> graph,
                                    int v, Deque<Integer> stack, int[] visiting) {
      if (visiting[v] == 2) return true;
      if (visiting[v] == 1) return false;

      visiting[v] = 1;
      for (Integer u : graph.getOrDefault(v, new HashSet<>())) {
        if (!topologicalSort(graph, u, stack, visiting)) {
          return false;
        }
      }
      visiting[v] = 2;
      stack.push(v);
      return true;
    }
  }

  public static void main(String[] args) {
    int[][] prerequisites = {{1, 0}, {2, 0}, {3, 1}, {3, 2}};
    Solution solution = new Solution();
    System.out.println(Arrays.toString(solution.findOrder(4, prerequisites)));
  }
}
