package me.yiyu.leetcode.week3;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class LC332ReconstructItinerary {

  static class Solution {
    public List<String> findItinerary(String[][] tickets) {
      Map<String, PriorityQueue<String>> flights = new HashMap<>();
      LinkedList<String> path = new LinkedList<>();

      // construct the graph
      for (String[] ticket: tickets) {
        flights.computeIfAbsent(ticket[0], k -> new PriorityQueue<>())
            .add(ticket[1]);
      }

      dfs(flights, "JFK", path);
      return path;
    }

    private void dfs(Map<String, PriorityQueue<String>> flights,
        String departure,
        LinkedList<String> path) {
      PriorityQueue<String> arrivals = flights.get(departure);
      while (arrivals != null && !arrivals.isEmpty()) {
        dfs(flights, arrivals.poll(), path);
      }
      path.addFirst(departure);
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    String[][] tickets = {
        {"JFK", "SFO"},
        {"JFK", "ATL"},
        {"SFO", "ATL"},
        {"ATL", "JFK"},
        {"ATL", "SFO"},
    };
    System.out.println(s.findItinerary(tickets));
  }

}
