package me.yiyu.leetcode.week3;

import java.util.ArrayList;
import java.util.List;

public class LC78Subsets {

  static class Solution {
    public List<List<Integer>> subsets(int[] nums) {
      List<List<Integer>> res = new ArrayList<>();
      List<Integer> item = new ArrayList<>();
      dfs(nums, 0, item, res);
      return res;
    }

    private void dfs(int[] nums, int start, List<Integer> item,
        List<List<Integer>> res) {
      res.add(new ArrayList<Integer>(item));
      for (int i = start; i < nums.length; i++) {
        item.add(nums[i]);
        dfs(nums, i+1, item, res);
        item.remove(item.size() - 1);
      }
    }
  }

  public static void main(String[] args) {
    int[] nums = new int[]{1, 2, 3};
    Solution s = new Solution();
    System.out.println(s.subsets(nums));
  }
}
