package me.yiyu.leetcode.week4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LC40CombinationSumII {
  static class Solution1 {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
      List<List<Integer>> res = new ArrayList<>();
      List<Integer> item = new ArrayList<>();
      Arrays.sort(candidates);
      dfs(candidates, target, 0, item, res);
      return res;
    }

    private void dfs(int[] candidates, int target, int start, List<Integer> item, List<List<Integer>> res) {
      if (target == 0) {
        res.add(new ArrayList<Integer>(item));
        return;
      }
      for (int i = start; i < candidates.length; i++) {
        if (candidates[i] > target) break;
        if (i > start && candidates[i] == candidates[i-1]) continue;
        item.add(candidates[i]);
        dfs(candidates, target - candidates[i], i+1, item, res);
        item.remove(item.size() - 1);
      }
    }
  }

  public static void main(String[] args) {
    int[] candidates = new int[] {10,1,2,7,6,1,5};
    int target = 8;
    Solution1 s1 = new Solution1();
    System.out.println(s1.combinationSum2(candidates, target));

  }

}
