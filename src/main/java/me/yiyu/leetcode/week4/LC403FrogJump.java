package me.yiyu.leetcode.week4;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LC403FrogJump {

  public class Solution1 {
    public boolean canCross(int[] stones) {
      int k = 0;
      return helper(stones, 0, k);
    }

    private boolean helper(int[] stones, int index, int k) {
      // 目前已经达到了
      if (index == stones.length - 1) return true;
      //选择k的步伐，范围k-1到k
      for (int i = k - 1; i <= k + 1; i++) {
        int nextJump = stones[index] + i;
        // 看接下来有没有合适的石头可以跳过去
        // 从接下来的位置中查找有没有nextJump的位置，有的话返回石头的编号
        int nextPosition = Arrays.binarySearch(stones, index + 1, stones.length, nextJump);
        if (nextPosition > 0) {
          if (helper(stones, nextPosition, i)) {
            return true;
          }
        }
      }

      return false;
    }
  }

  public class Solution2 {
    public boolean canCross(int[] stones) {
      if (stones.length == 0) return true;
      Map<Integer, Set<Integer>> map = new HashMap<>();
      for (int i = 0; i < stones.length; i++) {
        map.put(stones[i], new HashSet<Integer>());
      }
      map.get(0).add(1);
      for (int i = 0; i < stones.length - 1; i++) {
        int stone = stones[i];
        for (int step : map.get(stone)) {
          int reach = stone + step;
          if (reach == stones[stones.length - 1]) {
            return true;
          }
          Set<Integer> set = map.get(reach);
          if (set != null) {
            set.add(step);
            if (step - 1 > 0) set.add(step - 1);
            set.add(step + 1);
          }
        }
      }

      return false;
    }
  }

  public class Solution3 {
    public boolean canCross(int[] stones) {
      if (stones.length == 1 && stones[0] == 0) return true;
      if (stones[1] != 1) return false;

      boolean [][] dp = new boolean[stones.length][stones.length];
      dp[0][0] = true;
      for(int i = 0; i < stones.length; i++){
        for(int j = 0; j < i; j++){
          int k = stones[i] - stones[j];
          if(k >= stones.length){  //因为最大的步数就是stones.length
            continue;
          }
          dp[i][k] = dp[j][k];
          if(k - 1 >= 0){
            dp[i][k] = dp[i][k] || dp[j][k - 1];
          }
          if(k + 1 < stones.length){
            dp[i][k] = dp[i][k] || dp[j][k + 1];
          }

        }
      }
      for(int i = 0; i < stones.length;i++){
        if(dp[stones.length - 1 ][i]){
          return true;
        }
      }
      return false;
    }
  }




}
