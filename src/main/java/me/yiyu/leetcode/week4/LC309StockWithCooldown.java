package me.yiyu.leetcode.week4;

public class LC309StockWithCooldown {

  class Solution1 {
    public int maxProfit(int[] prices) {
      if (prices.length < 2) return 0;
      int[] s0 = new int[prices.length + 1];
      int[] s1 = new int[prices.length + 1];
      int[] s2 = new int[prices.length + 1];
      s0[0] = 0;
      s1[0] = Integer.MIN_VALUE;
      s2[0] = Integer.MIN_VALUE;
      for (int i = 1; i <= prices.length; i++) {
        s0[i] = Math.max(s0[i - 1], s2[i - 1]);
        s1[i] = Math.max(s1[i - 1], s0[i - 1] - prices[i-1]);
        s2[i] = s1[i - 1] + prices[i-1];
      }
      return Math.max(Math.max(s0[prices.length], s1[prices.length]) , s2[prices.length]);
    }
  }

  class Solution2 {
    public int maxProfit(int[] prices) {
      if (prices.length < 2) return 0;
      int s0 = 0;
      int s1 = Integer.MIN_VALUE;
      int s2 = Integer.MIN_VALUE;
      int ns0, ns1, ns2;
      for (int i = 1; i <= prices.length; i++) {
        ns0 = Math.max(s0, s2);
        ns1 = Math.max(s1, s0 - prices[i - 1]);
        ns2 = s1 + prices[i - 1];
        s0 = ns0; s1 = ns1; s2 = ns2;
      }
      return Math.max(Math.max(s0, s1) , s2);
    }
  }

}
