package me.yiyu.leetcode.week4;

import java.util.*;

public class LC377CombinationSumIV {
  class Solution1 {
    public int combinationSum4(int[] nums, int target) {
      int[] res = {0};
      List<Integer> item = new ArrayList<>();
      dfs(nums, target, 0, item, res);
      return res[0];
    }

    private void dfs(int[] nums, int target, int total,
        List<Integer> item, int[] res) {
      if (total > target) return;
      if (total == target) {
        res[0] += 1;
        return;
      }
      for (int i = 0; i < nums.length; ++i) {
        item.add(nums[i]);
        dfs(nums, target, total + nums[i], item, res);
        item.remove(item.size() - 1);
      }
    }
  }

  class Solution2 {
    Map<Integer, Integer> map = new HashMap<>();
    public int combinationSum4(int[] nums, int target) {
      int count = 0;
      if (nums == null || nums.length ==0 || target < 0 ) return 0;
      if ( target ==0 ) return 1;
      if (map.containsKey(target)) return map.get(target);
      for (int num: nums){
        count += combinationSum4(nums, target-num);
      }
      map.put(target, count);
      return count;
    }
  }

  class Solution3 {
    public int combinationSum4(int[] nums, int target) {
      int[] dp = new int[target + 1];
      Arrays.fill(dp, 0);
      dp[0] = 1;
      Arrays.sort(nums);
      for (int i = 1; i <= target; i++) {
        for (int num : nums) {
          if (num > i) {
            break;
          }
          dp[i] += dp[i - num];
        }
      }
      return dp[target];
    }
  }
}
