package me.yiyu.leetcode.week4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LC39CombinationSum {
  static class Solution1 {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
      List<List<Integer>> res = new ArrayList<>();
      List<Integer> item = new ArrayList<>();
      Arrays.sort(candidates);
      dfs(candidates, target, item, res);
      return res;
    }

    private void dfs(int[] candidates, int target, List<Integer> item, List<List<Integer>> res) {
      if (target == 0) {
        res.add(new ArrayList<Integer>(item));
        return;
      }
      for (int i = 0; i < candidates.length; i++) {
        if (candidates[i] > target) break;
        if (!item.isEmpty() && candidates[i] < item.get(item.size() - 1)) continue;
        item.add(candidates[i]);
        dfs(candidates, target - candidates[i], item, res);
        item.remove(item.size() - 1);
      }
    }
  }

  public static void main(String[] args) {
    int[] candidates = new int[] {2,3,5};
    int target = 8;
    Solution1 s1 = new Solution1();
    System.out.println(s1.combinationSum(candidates, target));
  }

}
