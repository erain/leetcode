package me.yiyu.leetcode.random;

public class LinkedLists {
  private LinkedLists() {}

  public static ListNode initSampleList() {
    ListNode head = new ListNode(0);
    ListNode curr = head;
    for (int i = 1; i <= 5; ++i) {
      ListNode temp = new ListNode(i);
      curr.next = temp;
      curr = temp;
    }
    return head;
  }

  public static void printList(ListNode node) {
    if (node == null) return;
    while (node.next != null) {
      System.out.print(node.val + " -> ");
      node = node.next;
    }
    System.out.println(node.val);
  }

  /**
   * Reverse a linkedlist between start / end
   * @param start start point for reversal, exclusive
   * @param end end point for reversal, exclusive
   * @return
   */
  public static ListNode reverseBetween(ListNode start, ListNode end) {
    return null;
  }
}
