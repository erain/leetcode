package me.yiyu.leetcode.random;

public class ReverseLinkedListII {
  class Solution {
    public ListNode reverseBetween(ListNode head, int m, int n) {
      // setup the dummy node
      ListNode dummy = new ListNode(0);
      dummy.next = head;

      // cur is the first position, pre is the node before that
      ListNode prev = dummy;

      // find the start point
      for (int i = 0; i < m-1; i++) {
        prev = prev.next;
      }
      ListNode reservedPrev = prev;

      // position m
      prev = prev.next;
      ListNode curr = prev.next;
      for (int i = m; i < n; i++) {
        prev.next = curr.next;
        curr.next = reservedPrev.next;
        reservedPrev.next = curr;
        curr = prev.next;
      }

      return dummy.next;
    }
  }
}
