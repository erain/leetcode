package me.yiyu.leetcode.random;

import com.google.common.primitives.Ints;
import java.util.List;

public class ListNode {
  public int val;
  public ListNode next;

  public ListNode(int x) { val = x; }

  public static ListNode makeList(List<Integer> elems) {
    ListNode dummyHead = new ListNode(0);
    ListNode cur = dummyHead;
    for (int e : elems) {
      cur.next = new ListNode(e);
      cur = cur.next;
    }
    return dummyHead.next;
  }

  public static ListNode makeList(int[] elems) {
    return makeList(Ints.asList(elems));
  }

  public static void printList(ListNode cur) {
    while (cur != null) {
      System.out.print(cur.val);
      if (cur.next != null) {
        System.out.print(" -> ");
      } else {
        System.out.println();
      }
      cur = cur.next;
    }
  }
}