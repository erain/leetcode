package me.yiyu.leetcode.random;

import java.util.Arrays;
import java.util.List;

public class CombinationSumIV {

  static class Solution {
    public int combinationSum4(int[] nums, int target) {
      Arrays.sort(nums);
      int[] res = new int[target+1];
      Arrays.fill(res, 0);
      res[0] = 1;
      dp(nums, target, res);
      System.out.println(Arrays.toString(res));
      return res[target];
    }

    private int dp(int[] nums, int target, int[] res) {
      if (res[target] != 0) return res[target];
      for (int i = 0; i < nums.length; ++i) {
        if (target - nums[i] < 0) break;
        res[target] += dp(nums, target - nums[i], res);
      }
      return res[target];
    }
  }
  public static void main(String[] args) {
    Solution s = new Solution();
    int[] nums = {1,2,3};
    int target = 4;
    int res = s.combinationSum4(nums, target);
    System.out.println(res);
  }


}
