package me.yiyu.leetcode.random;

import java.util.HashMap;
import java.util.Map;

public class PermutationInString {

  /*
      LC 567

      Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1. In other words, one of the first string's permutations is the substring of the second string.

      Example :
      Input:s1 = "ab" s2 = "eidbaooo"
      Output:True
      Explanation: s2 contains one permutation of s1 ("ba").
  */

  public static class Solution {
    public boolean checkInclusion(String s1, String s2) {
      if (s1 == null || s2 == null) return false;
      Map<Character, Integer> map = new HashMap<>();
      for (char c : s1.toCharArray()) {
        map.put(c, map.getOrDefault(c, 0) + 1);
      }
      int count = map.size();
      int begin = 0, end = 0;
      while (end < s2.length()) {
        char ch = s2.charAt(end);
        if (map.containsKey(ch)) {
          map.put(ch, map.get(ch) - 1);
          if (map.get(ch) == 0) {
            count--;
          }
        }
        while (count == 0) {
          if (end - begin + 1 == s1.length()) {
            return true;
          }
          char temp = s2.charAt(begin);
          if (map.containsKey(temp)) {
            map.put(temp, map.get(temp) + 1);
            if (map.get(temp) > 0) {
              count++;
            }
          }
          begin++;
        }
        end++;
      }
      return false;
    }
  }

  public static void main(String[] args) {
    Solution solution = new Solution();
    System.out.println(solution.checkInclusion("abc", "bbbbbcccaa"));
  }
}
