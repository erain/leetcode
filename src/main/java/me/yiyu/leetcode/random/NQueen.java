package me.yiyu.leetcode.random;

public class NQueen {
  public static int res = 0;

  public static int totalNQueens(int n) {
    if (n <= 0) {
      return 0;
    }

    int[] col = new int[n];
    DFS(n, 0, col);
    return res;
  }

  public static void DFS(int n, int row, int[] col) {
    if (row == n) {
      res += 1;
    } else {
      for (int i = 0; i < n; i++) {
        col[row] = i;  // (row, col[row]) => (row, i)
        if ( isValid(row, col) ) {
          DFS(n, row+1, col);
        }
      }
    }
  }

  public static boolean isValid(int row, int[] col) {
    for (int i = 0; i < row; i++) {
      if (col[row] == col[i]
          || Math.abs(col[row] - col[i]) == row - i) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    System.out.println(totalNQueens(4));
  }


}
