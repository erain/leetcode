package me.yiyu.leetcode.random;

public class ReverseLinkedList {
  static class Solution {
    public ListNode reverseList(ListNode head) {
      ListNode prev = null, curr = head;
      while (curr != null) {
        ListNode temp = curr.next;
        curr.next = prev;
        prev = curr;
        curr = temp;
      }
      head = prev;
      return head;
    }

    public ListNode reverseListRec(ListNode curr) {
      if (curr == null || curr.next == null) return curr;
      ListNode nextNode = curr.next;
      curr.next = null;
      ListNode reverseRest = reverseListRec(nextNode);
      nextNode.next = curr;
      return reverseRest;
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();

    // non-recursion
    ListNode head = LinkedLists.initSampleList();
    head = s.reverseList(head);
    LinkedLists.printList(head);

    // recursion
    head = LinkedLists.initSampleList();
    head = s.reverseListRec(head);
    LinkedLists.printList(head);
  }
}
