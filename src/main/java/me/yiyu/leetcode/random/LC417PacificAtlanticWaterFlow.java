package me.yiyu.leetcode.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LC417PacificAtlanticWaterFlow {

}

class Solution {

  public List<int[]> pacificAtlantic(int[][] matrix) {
    if (matrix.length == 0 || matrix[0].length == 0) {
      return new ArrayList<int[]>();
    }
    int m = matrix.length;
    int n = matrix[m - 1].length;
    int[][] canFlowP = new int[m][n];
    int[][] canFlowA = new int[m][n];
    for (int i = 0; i < n; i++) {
      canFlowDFS(0, i, m, n, 0, canFlowP, matrix);
      canFlowDFS(m - 1, i, m, n, 0, canFlowA, matrix);
    }
    for (int i = 0; i < m; i++) {
      canFlowDFS(i, 0, m, n, 0, canFlowP, matrix);
      canFlowDFS(i, n - 1, m, n, 0, canFlowA, matrix);
    }
    List<int[]> result = new ArrayList<>();
    findIntersection(m, n, canFlowP, canFlowA, result);
    return result;
  }

  private void canFlowDFS(int i, int j, int m, int n, int height, int[][] canFlow, int[][]
      matrix) {
    if (i < 0 || i >= m || j < 0 || j >= n || canFlow[i][j] <= -4 || canFlow[i][j] == 1) {
      return;
    }
    if (matrix[i][j] < height) {
      canFlow[i][j]--;
      return;
    }
    canFlow[i][j] = 1;
    int[] dirX = {-1, 0, 1, 0};
    int[] dirY = {0, -1, 0, 1};
    for (int k = 0; k < 4; k++) {
      canFlowDFS(i + dirX[k], j + dirY[k], m, n, matrix[i][j], canFlow, matrix);
    }
  }

  private void findIntersection(int m, int n, int[][] canFlowP, int[][] canFlowA,
      List<int[]> result) {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (canFlowP[i][j] == 1 && canFlowA[i][j] == 1) {
          result.add(new int[]{i, j});
        }
      }
    }
  }
}

