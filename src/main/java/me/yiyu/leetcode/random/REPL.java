package me.yiyu.leetcode.random;

import java.util.PriorityQueue;

public class REPL {

  public static void main(String[] args) {
    int[] arr = {3,2,1,5,6,4};
    PriorityQueue<Integer> pq = new PriorityQueue<>();
    for (int i : arr) {
      pq.offer(i);
      System.out.println(pq.peek());
    }

  }

}
