package me.yiyu.leetcode.random;

import java.util.HashMap;

public class MinimumWindowSubtstring {

  static class Solution {

    public String minWindow(String S, String T) {
      String res = "";
      if (S == null || T == null || S.length() == 0 || T.length() == 0) {
        return res;
      }

      // construct the target map
      HashMap<Character, Integer> map = new HashMap<>();
      for (char c : T.toCharArray()) {
        map.put(c, map.getOrDefault(c, 0) + 1);
      }
      int count = map.size();
      int minLen = S.length() + 1;
      int begin = 0, end = 0;
      // move the end of the sliding window
      while (end < S.length()) {
        char ch = S.charAt(end);
        if (map.containsKey(ch)) {
          map.put(ch, map.get(ch) - 1);
          if (map.get(ch) == 0) {
            count--;
          }
        }
        // reach to the maximum window
        while (count == 0) {
          // find the possible solution
          if (minLen > end - begin + 1) {
            res = S.substring(begin, end + 1);
            minLen = res.length();
          }
          // move the begin of the sliding window
          char temp = S.charAt(begin);
          if (map.containsKey(temp)) {
            map.put(temp, map.get(temp) + 1);
            if (map.get(temp) > 0) {
              count++;
            }
          }
          begin++;
        }
        end++;
      }
      return res;
    }
  }


  public static void main(String[] args) {
    String S = "ADOBECODEBANC";
    String T = "ABC";
    Solution solution = new Solution();
    System.out.println(solution.minWindow(S, T));
  }
}
