package me.yiyu.leetcode.random;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {
  public int val;
  public TreeNode left, right;
  public TreeNode(int val) {
    this.val = val;
  }

  @Override
  public String toString() {
    return "TreeNode{" +
        "val=" + val +
        ", left=" + left +
        ", right=" + right +
        '}';
  }

  public static TreeNode getSimpleTree() {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.left = new TreeNode(4);
    root.left.left.left = new TreeNode(6);
    root.left.left.right = new TreeNode(8);
    root.left.left.left.right = new TreeNode(7);
    return root;
  }

  public static TreeNode getSymmetricTree() {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(2);
    root.left.left = new TreeNode(3);
    root.right.right = new TreeNode(3);
    root.left.right = new TreeNode(4);
    root.right.left = new TreeNode(4);
    return root;
  }

  public static List<List<Integer>> findPaths(TreeNode root) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> partialPath = new ArrayList<>();
    findPaths(root, partialPath, res );
    return res;
  }

  private static void findPaths(TreeNode node, List<Integer> partialPath, List<List<Integer>> res) {
    if (node == null) return;

    partialPath.add(node.val);

    if (node.left == null && node.right == null) {
      res.add(new ArrayList<>(partialPath));
    } else {
      findPaths(node.left, partialPath, res);
      findPaths(node.right, partialPath, res);
    }

    partialPath.remove(partialPath.size() - 1);
  }
}
