package me.yiyu.leetcode.random;

import java.util.ArrayList;
import java.util.List;

public class CombinationSumI {
  public static int[] stringToIntegerArray(String input) {
    input = input.trim();
    input = input.substring(1, input.length() - 1);
    if (input.length() == 0) {
      return new int[0];
    }

    String[] parts = input.split(",");
    int[] output = new int[parts.length];
    for(int index = 0; index < parts.length; index++) {
      String part = parts[index].trim();
      output[index] = Integer.parseInt(part);
    }
    return output;
  }

  public static String integerArrayListToString(List<Integer> nums, int length) {
    if (length == 0) {
      return "[]";
    }

    String result = "";
    for(int index = 0; index < length; index++) {
      Integer number = nums.get(index);
      result += Integer.toString(number) + ", ";
    }
    return "[" + result.substring(0, result.length() - 2) + "]";
  }

  public static String integerArrayListToString(List<Integer> nums) {
    return integerArrayListToString(nums, nums.size());
  }

  public static String int2dListToString(List<List<Integer>> nums) {
    StringBuilder sb = new StringBuilder("[");
    for (List<Integer> list: nums) {
      sb.append(integerArrayListToString(list));
      sb.append(",");
    }

    sb.setCharAt(sb.length() - 1, ']');
    return sb.toString();
  }


  static public List<List<Integer>> combinationSum(int[] candidates, int target) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> item = new ArrayList<>();
    dfs(candidates, target, 0, item, res);
    return res;
  }

  static private void dfs(int[] candidates, int target, int sum, List<Integer> item, List<List<Integer>> res) {

    if (sum > target) {
      return;
    }
    if (sum == target) {
      res.add(new ArrayList<Integer>(item));
      return;
    }
    for (int i = 0; i < candidates.length; i++) {
      if (!item.isEmpty() && candidates[i] < item.get(item.size() - 1)) {
        continue;
      }
      item.add(candidates[i]);
      dfs(candidates, target, sum + candidates[i], item, res);
      item.remove(item.size() - 1);
    }
  }

  public static void main(String[] args) {
    int[] candidates = {1,2};
    int target = 4;
    List<List<Integer>> res = combinationSum(candidates, target);
    System.out.println(int2dListToString(res));
  }

}
