package me.yiyu.leetcode.week2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import me.yiyu.leetcode.random.TreeNode;

public class LC94BinaryTreeInorderTraversal {
  class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
      List<Integer> ret = new ArrayList<>();
      if (root == null) return ret;
      Stack<TreeNode> stack = new Stack<>();
      pushLeftNodes(root, stack);
      while (!stack.isEmpty()) {
        TreeNode top = stack.pop();
        ret.add(top.val);
        pushLeftNodes(top.right, stack);
      }
      return ret;
    }

    private void pushLeftNodes(TreeNode root, Stack<TreeNode> stack) {
      while (root != null) {
        stack.push(root);
        root = root.left;
      }
    }
  }

}
