package me.yiyu.leetcode.week2;

import java.util.Comparator;
import java.util.List;
import me.yiyu.leetcode.random.TreeNode;

public class LC111MinimumDepthofBinaryTree {
  static class Solution {
    public int minDepth(TreeNode root) {
      if (root == null) return 0;
      if (root.left == null && root.right == null) return 1;
      if (root.left == null) return 1 + minDepth(root.right);
      if (root.right == null) return 1 + minDepth(root.left);
      return 1 + Math.min(minDepth(root.left), minDepth(root.right));
    }
  }

  public static void main(String[] args) {
    TreeNode root = TreeNode.getSimpleTree();
    Solution s = new Solution();
    System.out.println(s.minDepth(root));
    List<List<Integer>> paths = TreeNode.findPaths(root);
    System.out.println(paths);
    int s2 = paths.stream()
        .min(Comparator.comparingInt(List::size))
        .get().size();
    System.out.println(s2);
  }
}
