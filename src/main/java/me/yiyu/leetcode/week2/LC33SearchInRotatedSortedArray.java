package me.yiyu.leetcode.week2;

public class LC33SearchInRotatedSortedArray {

  static class Solution {

    public int search(int[] nums, int target) {
      int start = 0, end = nums.length - 1;
      while (start <= end) {
        int mid = start + (end - start) / 2;

        // we find the answer
        if (nums[mid] == target) {
          return mid;
        }

        // situation 1
        if (nums[start] <= nums[mid]) {
          if (target >= nums[start] && target < nums[mid]) {
            end = mid - 1;
          } else {
            start = mid + 1;
          }
        }

        // situation 2
        if (nums[mid] <= nums[end]) {
          if (target > nums[mid] && target <= nums[end]) {
            start = mid + 1;
          } else {
            end = mid - 1;
          }
        }
      }
      return -1;
    }
  }

  public static void main(String[] args) {
    int[] nums = new int[]{4, 5, 6, 7, 0, 1, 2};
    int target = 6;

    Solution s = new Solution();
    System.out.println(s.search(nums, target));
  }
}
