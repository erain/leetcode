package me.yiyu.leetcode.week2;

import me.yiyu.leetcode.random.TreeNode;

public class LC124BinaryTreeMaximumPathSum {
  class Solution {
    public int maxPathSum(TreeNode root) {
      if (root == null) return 0;
      int[] globalMax = new int[1];
      globalMax[0] = root.val;
      maxPathSum(root, globalMax);
      return globalMax[0];
    }

    private int maxPathSum(TreeNode root, int[] globalMax) {
      if (root == null) return 0;
      int left = maxPathSum(root.left, globalMax);
      int right = maxPathSum(root.right, globalMax);
      int maxForUpperlevel = Math.max(
          root.val, Math.max(root.val + left, root.val + right));
      int nodeMax = Math.max(maxForUpperlevel, root.val + left + right);
      globalMax[0] = Math.max(globalMax[0], nodeMax);
      return maxForUpperlevel;
    }

  }
}
