package me.yiyu.leetcode.week2;

import java.util.List;
import me.yiyu.leetcode.random.TreeNode;

public class LC129SumRootToLeafNumbers {
  static class Solution1 {
    public int sumNumbers(TreeNode root) {
      if (root == null) return 0;
      int[] sum = new int[1];
      dfs(root, root.val, sum);
      return sum[0];
    }

    public void dfs(TreeNode root, int num, int[] sum) {
      if (root.left == null && root.right == null) {
        sum[0] += num;
        return;
      }

      if (root.left != null) {
        dfs(root.left, num * 10 + root.left.val, sum);
      }

      if (root.right != null) {
        dfs(root.right, num * 10 + root.right.val, sum);
      }

    }
  }

  static class Solution {
    public int sumNumbers(TreeNode root) {
      return sum(root, 0);
    }

    public int sum(TreeNode n, int s) {
      if (n == null) return 0;
      if (n.right == null && n.left == null) return s * 10 + n.val;
      return sum(n.left, s*10 + n.val) + sum(n.right, s * 10 + n.val);
    }
  }

  public static void main(String[] args) {
    TreeNode root = TreeNode.getSimpleTree();
    Solution s = new Solution();
    System.out.println(s.sumNumbers(root));
    List<List<Integer>> paths = TreeNode.findPaths(root);
    System.out.println(paths);
    int s2 = paths.stream()
        .map(p -> p.stream().reduce(0, (x, y) -> 10 * x + y))
        .reduce(0, Integer::sum);
    System.out.println(s2);
  }
}
