package me.yiyu.leetcode.week2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import me.yiyu.leetcode.random.TreeNode;

public class LC257BinaryTreePaths {

  static class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
      List<String> res = new ArrayList<>();
      findPaths(root, "", res);
      return res;
    }

    private void findPaths(TreeNode node, String path, List<String> res) {
      if (node == null) {
        return;
      }

      if (node.left == null && node.right == null) {
        res.add(path + node.val);
        return;
      }

      path += node.val + "->";
      findPaths(node.left, path, res);
      findPaths(node.right, path, res);
    }
  }

  public static void main(String[] args) {
    TreeNode root = TreeNode.getSimpleTree();
    Solution s = new Solution();
    System.out.println(s.binaryTreePaths(root));
    List<List<Integer>> paths = TreeNode.findPaths(root);
    System.out.println(paths);
    List<String> s2 = paths.stream()
        .map(p -> p.stream().map(Object::toString).collect(Collectors.toList()))
        .map(p -> String.join("->", p))
        .collect(Collectors.toList());
    System.out.println(s2);
  }

}
