package me.yiyu.leetcode.week2;

import me.yiyu.leetcode.random.TreeNode;

public class LC101SymmetricTree {

  static class Solution {
    public boolean isSymmetric(TreeNode root) {
      if (root == null) return true;
      return isSymmetric(root.left, root.right);
    }

    private boolean isSymmetric(TreeNode node1, TreeNode node2) {
      if (node1 == null && node2 == null) return true;
      if (node1 == null || node2 == null) return false;
      if (node1.val != node2.val)  return false;
      return isSymmetric(node1.left, node2.right) &&
          isSymmetric(node1.right, node2.left);
    }
  }

  public static void main(String[] args) {
    TreeNode root = TreeNode.getSymmetricTree();
    Solution s = new Solution();
    System.out.println(s.isSymmetric(root));
    root = TreeNode.getSimpleTree();
    System.out.println(s.isSymmetric(root));
  }
}
