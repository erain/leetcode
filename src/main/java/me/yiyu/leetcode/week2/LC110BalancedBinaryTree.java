package me.yiyu.leetcode.week2;

import me.yiyu.leetcode.random.TreeNode;

public class LC110BalancedBinaryTree {

  static class Solution {
    public boolean isBalanced(TreeNode root) {
      if (root == null) {
        return true;
      }
      return checkDepth(root) != -1;
    }

    // return -1 if not balanced
    // return the max(left, right) + 1 as the depth of root
    public int checkDepth(TreeNode root) {
      if (root == null) return 0;
      int leftDepth = checkDepth(root.left);
      int rightDepth = checkDepth(root.right);
      if (leftDepth == -1 || rightDepth == -1
          || Math.abs(leftDepth - rightDepth) > 1) {
        return -1;
      } else {
        return Math.max(leftDepth, rightDepth) + 1;
      }
    }
  }
}
