package me.yiyu.leetcode.week2;

import me.yiyu.leetcode.random.TreeNode;

public class LC222CountCompleteTreeNodes {
  static class Solution {
    public int countNodes1(TreeNode root) {
      if (root == null) return 0;
      int leftmost = 0;
      TreeNode left = root;
      while (left != null) {
        leftmost++;
        left = left.left;
      }
      int rightmost = 0;
      TreeNode right = root;
      while (right != null) {
        rightmost++;
        right = right.right;
      }
      if (leftmost == rightmost) return (1 << leftmost) - 1;
      return 1 + countNodes1(root.left) + countNodes1(root.right);
    }

    public int countNodes(TreeNode root) {
      if (root == null) return 0;
      int height = getHeight(root);
      return countNodes(root, height);
    }

    private int countNodes(TreeNode root, int height) {
      if (root == null) return 0;
      int rightHeight = getHeight(root.right);
      if (rightHeight == height - 1) {
        return (1 << rightHeight) + countNodes(root.right, height - 1);
      } else {
        return (1 << rightHeight) + countNodes(root.left, height - 1);
      }
    }

    private int getHeight(TreeNode root) {
      int res = 0;
      while (root != null) {
        res++;
        root = root.left;
      }
      return res;
    }

  }

  static class Solution2 {
    public int countNodes(TreeNode root) {
      if (root == null) return 0;
      int lh = getHeight(root.left);
      int rh = getHeight(root.right);
      if (lh == rh) {
        return (1 << lh) + countNodes(root.right);
      } else {
        return (1 << rh) + countNodes(root.left);
      }
    }

    private int getHeight(TreeNode root) {
      if (root == null) return 0;
      return 1 + getHeight(root.left);
    }
  }


  public static void main(String[] args) {
    Solution s = new Solution();
    TreeNode root = TreeNode.getSymmetricTree();
    Solution2 s2 = new Solution2();
    System.out.println(s.countNodes1(root));
    System.out.println(s.countNodes(root));
    System.out.println(s2.countNodes(root));
    TreeNode leftMost = root;
    while (leftMost.left != null) {
      leftMost = leftMost.left;
    }
    leftMost.left = new TreeNode(5);
    leftMost.right = new TreeNode(6);
    System.out.println(s.countNodes1(root));
    System.out.println(s.countNodes(root));
    System.out.println(s2.countNodes(root));
  }
}
