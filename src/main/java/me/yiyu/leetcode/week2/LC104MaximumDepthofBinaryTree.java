package me.yiyu.leetcode.week2;

import java.util.Comparator;
import java.util.List;
import me.yiyu.leetcode.random.TreeNode;

public class LC104MaximumDepthofBinaryTree {

  static class Solution {
    public int maxDepth(TreeNode root) {
      if (root == null) return 0;
      if (root.left == null && root.right == null) return 1;
      return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }
  }

  public static void main(String[] args) {
    TreeNode root = TreeNode.getSimpleTree();
    Solution s = new Solution();
    System.out.println(s.maxDepth(root));
    List<List<Integer>> paths = TreeNode.findPaths(root);
    System.out.println(paths);
    int s2 = paths.stream()
        .max(Comparator.comparingInt(List::size))
        .get().size();
    System.out.println(s2);
  }

}
