package me.yiyu.leetcode.week2;

import me.yiyu.leetcode.random.TreeNode;

public class LC156BinaryTreeUpsideDown {

  static class Solution1 {

    public TreeNode upsideDownBinaryTree(TreeNode root) {
      if (root == null || root.left == null) {
        return root;
      }
      TreeNode newRoot = upsideDownBinaryTree(root.left);
      root.left.left = root.right;
      root.left.right = root;
      root.left = null;
      root.right = null;
      return newRoot;
    }
  }

  static class Solution2 {

    public TreeNode upsideDownBinaryTree(TreeNode root) {
      TreeNode cur = root;
      TreeNode pre = null;
      TreeNode next = null;
      TreeNode tmp = null;
      while (cur != null) {
        next = cur.left;
        // tmp: keep the previous right node
        cur.left = tmp;
        tmp = cur.right;

        cur.right = pre;
        pre = cur;
        cur = next;
      }

      return pre;
    }
  }

  private static TreeNode getSampleTree() {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    return root;
  }

  public static void main(String[] args) {

    Solution1 s1 = new Solution1();
    TreeNode newRoot = s1.upsideDownBinaryTree(getSampleTree());
    System.out.println(newRoot);

    Solution2 s2 = new Solution2();
    TreeNode newRoot2 = s2.upsideDownBinaryTree(getSampleTree());
    System.out.println(newRoot2);
  }
}
