package me.yiyu.leetcode.week6;

public class LC378KthSmallestElementInASortedMatrix {
  class Solution1 {
    public int kthSmallest(int[][] matrix, int k) {
      //[lo, hi)
      int M = matrix.length, N = matrix[0].length;
      int lo = matrix[0][0];
      int hi = matrix[M - 1][N - 1] + 1;
      while(lo < hi) {
        int mid = lo + (hi - lo) / 2, count = 0;
        for(int i = 0; i < M; i++)
          for (int j = 0; j < N; j++)
            if (matrix[i][j] <= mid) count++;
        if(count < k) lo = mid + 1;
        else hi = mid;
      }
      return lo;
    }
  }

  public class Solution2 {
    public int kthSmallest(int[][] matrix, int k) {
      //[lo, hi)
      int M = matrix.length, N = matrix[0].length;
      int lo = matrix[0][0];
      int hi = matrix[M - 1][N - 1] + 1;
      while(lo < hi) {
        int mid = lo + (hi - lo) / 2;
        int count = 0, j = N - 1;
        for(int i = 0; i < M; i++) {
          while(j >= 0 && matrix[i][j] > mid) j--;
          count += (j + 1);
        }
        if(count < k) lo = mid + 1;
        else hi = mid;
      }
      return lo;
    }
  }
}
