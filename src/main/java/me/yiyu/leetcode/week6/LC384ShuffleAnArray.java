package me.yiyu.leetcode.week6;

import java.util.Random;

class LC384ShuffleAnArray {
  private int[] array;
  private int[] original;

  Random rand = new Random();

  private int randRange(int min, int max) {
    return rand.nextInt(max - min) + min;
  }

  private void swapAt(int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  public LC384ShuffleAnArray(int[] nums) {
    array = nums;
    original = nums.clone();
  }

  public int[] reset() {
    array = original;
    original = original.clone();
    return original;
  }

  public int[] shuffle() {
    for (int i = 0; i < array.length; i++) {
      swapAt(i, randRange(i, array.length));
    }
    return array;
  }
}