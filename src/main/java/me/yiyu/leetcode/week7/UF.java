package me.yiyu.leetcode.week7;

public class UF {
  int[] parent;

  public UF(int N) {
    parent = new int[N];
    for (int i = 0; i < N; i++) parent[i] = i;
  }

  public int find(int x) {
    if (parent[x] != x) parent[x] = find(parent[x]);
    return parent[x];
  }

  public void union(int x, int y) {
    parent[find(x)] = find(y);
  }
}
