package me.yiyu.leetcode.week7;

public class LC839SimilarStringGroups {

  class Solution1 {
    public int numSimilarGroups(String[] A) {
      if(A.length < 2) return A.length;
      int res = 0;
      for(int i=0;i<A.length;i++){
        if(A[i] == null) continue;
        String str = A[i];
        A[i] = null;
        res++;
        dfs(A,str);
      }
      return res;
    }
    public void dfs(String[] arr,String str){
      for(int i=0;i<arr.length;i++){
        if(arr[i] == null) continue;
        if(similar(str,arr[i])){// both string str and arr[i] belong in same group
          String s = arr[i];
          arr[i] = null;
          dfs(arr,s);
        }
      }
    }
    private boolean similar(String w1, String w2) {
      int diff = 0;
      for (int i = 0; i < w1.length(); i++) {
        if (w1.charAt(i) != w2.charAt(i)) {
          diff++;
          if (diff > 2) break;
        }
      }
      return diff <= 2;
    }
  }

  class Solution2 {
    public int numSimilarGroups(String[] A) {
      int N = A.length;      // total number of strings
      UF uf = new UF(N);

      for (int i = 0; i < N; i++)
        for (int j = i+1; j < N; j++)
          if (similar(A[i], A[j])) uf.union(i, j);

      int ret = 0;
      for (int i = 0; i < N; i++)
        if (uf.find(i) == i) ret++;
      return ret;
    }

    private boolean similar(String w1, String w2) {
      int diff = 0;
      for (int i = 0; i < w1.length(); i++) {
        if (w1.charAt(i) != w2.charAt(i)) {
          diff++;
          if (diff > 2) break;
        }
      }
      return diff <= 2;
    }

  }
}
