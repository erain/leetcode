package me.yiyu.leetcode.unionfind;

public class QuickUnionUF {
  private int[] parents;

  public QuickUnionUF(int N) {
    parents = new int[N];
    for (int i = 0; i < N; i++) parents[i] = i;
  }

  private int find(int i) {
    while (i != parents[i]) i = parents[i];
    return i;
  }

  public boolean connected(int p, int q) {
    return find(p) == find(q);
  }

  public void union(int p, int q) {
    int i = find(p);
    int j = find(q);
    parents[i] = j;
  }
}
