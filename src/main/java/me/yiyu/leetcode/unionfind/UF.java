package me.yiyu.leetcode.unionfind;

public class UF {

  private int[] parent;  // parent[i] = parent of i
  private byte[] rank;   // rank[i] = rank of subtree rooted at i (never more than 31
  private int count;     // number of components

  public UF(int n) {
    count = n;
    parent = new int[n];
    rank = new byte[n];
    for (int i = 0; i < n; i++) {
      parent[i] = i;
      rank[i] = 0;
    }
  }

  public int find(int p) {
    while (p != parent[p]) {
      parent[p] = parent[parent[p]];  // path compression by halving
      p = parent[p];
    }
    return p;
  }

  public int count() {
    return count;
  }

  public boolean connected(int p, int q) {
    return find(p) == find(q);
  }

  public void union(int p, int q) {
    int rootP = find(p);
    int rootQ = find(q);
    if (rootP == rootQ) return;

    if      (rank[rootP] < rank[rootQ]) parent[rootP] = rootQ;
    else if (rank[rootP] > rank[rootQ]) parent[rootQ] = rootP;
    else {
      parent[rootQ] = rootP;
      rank[rootP]++;
    }
    count--;
  }


}
