package me.yiyu.leetcode.week1;

import java.util.*;

public class LC71SimplifyPath {
  static class Solution {
    public String simplifyPath(String path) {
      Stack<String> stack = new Stack<>();
      Set<String> skip = new HashSet<>(Arrays.asList("..",".",""));
      for (String dir : path.split("/")) {
        if (dir.equals("..") && !stack.isEmpty()) {
          stack.pop();
        } else if (!skip.contains(dir)) {
          stack.push(dir);
        }
      }
      return "/" + String.join("/", stack);
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.simplifyPath("/home/"));
    System.out.println(s.simplifyPath("/a/./b/../../c/"));
    System.out.println(s.simplifyPath("/.."));
    System.out.println(s.simplifyPath("/abc/..."));
  }
}
