package me.yiyu.leetcode.week1;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;

public class LC57InsertInterval {

  static class Interval {
    int start;
    int end;

    Interval() {
      start = 0;
      end = 0;
    }

    Interval(int s, int e) {
      start = s;
      end = e;
    }

    @Override
    public String toString() {
      return "Interval{" +
          "start=" + start +
          ", end=" + end +
          '}';
    }
  }


  static class Solution {

    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
      List<Interval> res = new ArrayList<>();
      int index = 0, size = intervals.size();

      // before merge
      while (index < size && newInterval.start > intervals.get(index).end) {
        res.add(intervals.get(index++));
      }

      // during merge
      while (index < size && newInterval.end >= intervals.get(index).start) {
        newInterval = new Interval(
          Math.min(newInterval.start, intervals.get(index).start),
          Math.max(newInterval.end, intervals.get(index).end)
        );
        index++;
      }
      res.add(newInterval);

      // after merge
      while (index < size) {
        res.add(intervals.get(index++));
      }

      return res;
    }
  }

  public static void main(String[] args) {
    List<Interval> intervals = Lists.newArrayList(new Interval(1, 3), new Interval(6, 9));
    Interval newInterval = new Interval(2, 5);
    Solution solution = new Solution();
    System.out.println(solution.insert(intervals, newInterval));
  }


}



