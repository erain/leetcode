package me.yiyu.leetcode.week1;

import java.util.Arrays;

public class LC16ThreeSumClosest {

  static public class Solution {
    public int threeSumClosest(int[] nums, int target) {
      int result = nums[0] + nums[1] + nums[nums.length - 1];
      Arrays.sort(nums);
      for (int i = 0; i < nums.length - 1; i++) {
        int start = i + 1, end = nums.length - 1;
        while (start < end) {
          int sum = nums[i] + nums[start] + nums[end];
          if (sum > target) {
            end--;
          } else {
            start++;
          }
          if (Math.abs(sum - target) < Math.abs(result - target)) {
            result = sum;
          }
        }
      }
      return result;
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    int[] nums = new int[] {1, 1, -1, -1, 3};
    int target = 1;
    System.out.println(s.threeSumClosest(nums, target));
  }
}
