package me.yiyu.leetcode.week1;


import me.yiyu.leetcode.random.ListNode;

public class LC82RemoveDuplicatesFromSortedListII {

  static class Solution {
    public ListNode deleteDuplicates(ListNode head) {
      ListNode dummyHead = new ListNode(0);
      dummyHead.next = head;
      ListNode pre = dummyHead, cur = head;
      while (cur != null) {
        while (cur.next != null && cur.val == cur.next.val) {
          cur = cur.next;
        }
        if (pre.next == cur) {
          pre = pre.next;
        } else {
          pre.next = cur.next;
        }
        cur = cur.next;
      }
      return dummyHead.next;
    }
  }


  public static void main(String[] args) {
    ListNode head = ListNode.makeList(new int[] {1,1,1,2,3,3,4,4,5});
    Solution s = new Solution();
    ListNode.printList(s.deleteDuplicates(head));
  }
}

