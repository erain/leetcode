package me.yiyu.leetcode.week1;

import java.util.HashMap;
import java.util.Map;

public class LC246StrobogrammaticNumber {
  static class Solution {
    HashMap<Character, Character> map =
        new HashMap<Character, Character>() {{
      put('6', '9');
      put('9', '6');
      put('0', '0');
      put('1', '1');
      put('8', '8');
    }};

    public boolean isStrobogrammatic(String num) {
      int l = 0, r = num.length() - 1;
      while (l <= r) {
        if (!map.containsKey(num.charAt(l))) {
          return false;
        }
        if (map.get(num.charAt(l)) != num.charAt(r)) {
          return false;
        }
        l++;
        r--;
      }
      return true;
    }

    public boolean isStrobogrammatic2(String num) {
      if (num.length() == 0) {
        return true;
      }
      if (num.length() == 1) {
        if (num.equals("0") || num.equals("1") || num.equals("8")) {
          return true;
        }
        return false;
      }
      return map.get(num.charAt(0)).equals(num.charAt(num.length()-1)) &&
          isStrobogrammatic2(num.substring(1, num.length()-1));
    }
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.isStrobogrammatic("818"));
    System.out.println(s.isStrobogrammatic2("818"));
    System.out.println(s.isStrobogrammatic("118"));
    System.out.println(s.isStrobogrammatic2("118"));
    System.out.println(s.isStrobogrammatic("1"));
    System.out.println(s.isStrobogrammatic2("1"));
    System.out.println(s.isStrobogrammatic("4"));
    System.out.println(s.isStrobogrammatic2("4"));
    System.out.println(s.isStrobogrammatic(""));
    System.out.println(s.isStrobogrammatic2(""));
    System.out.println(s.isStrobogrammatic("69"));
    System.out.println(s.isStrobogrammatic2("69"));
  }
}
