package me.yiyu.leetcode.week5;

public class LC41FirstMissiongPositive {
  class Solution {
    public int firstMissingPositive(int[] nums) {
      if (nums == null || nums.length == 0) {
        return 1;
      }
      for (int i = 0; i < nums.length; ++i) {
        while (nums[i] <= nums.length && nums[i] > 0 &&
            nums[i] != nums[nums[i] - 1]) {
          swap(nums, i, nums[i] - 1);
        }
      }
      for (int i = 0; i < nums.length; ++i) {
        if (nums[i] != i + 1) {
          return i + 1;
        }
      }
      return nums.length + 1;
    }
    private void swap(int[] nums, int a, int b) {
      int temp = nums[a];
      nums[a] = nums[b];
      nums[b] = temp;
    }
  }
}
