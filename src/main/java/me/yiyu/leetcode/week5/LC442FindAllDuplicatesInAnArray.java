package me.yiyu.leetcode.week5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LC442FindAllDuplicatesInAnArray {
  static class Solution1 {
    public List<Integer> findDuplicates(int[] nums) {
      List<Integer> res = new ArrayList<>();
      for (int i = 0; i < nums.length; i++) {
        int index = Math.abs(nums[i]) - 1;
        if (nums[index] < 0) {
          res.add(index + 1);
        }
        nums[index] = - nums[index];
      }
      return res;
    }

    // follow up 1: what if after the function is returned, the nums remains the same
    // follow up 2: what if the element can be repeated more than twice.
  }

  static class Solution2 {
    public List<Integer> findDuplicates(int[] nums) {
      for(int i = 0; i < nums.length; i++) {
        // put nums[i] to it's right place if the right place is not already nums[i]
        while(i != nums[i]-1 && nums[i] != nums[nums[i]-1]) {
          int temp = nums[i];
          nums[i] = nums[nums[i]-1];
          nums[temp-1] = temp;
        }
      }
      List<Integer> result = new ArrayList<Integer>();
      for(int i = 0; i < nums.length; i++) {
        if(i != nums[i]-1) result.add(nums[i]);
      }
      return result;
    }
  }

  public static void main(String[] args) {
    int[] nums = new int[] {4,3,2,7,8,2,3,1};
    Solution1 s1 = new Solution1();
    System.out.println(s1.findDuplicates(nums));
    System.out.println(Arrays.toString(nums));

    nums = new int[] {4,3,2,7,8,2,3,1};
    Solution2 s2 = new Solution2();
    System.out.println(s2.findDuplicates(nums));
    System.out.println(Arrays.toString(nums));
  }

}
