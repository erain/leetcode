package me.yiyu.leetcode.week5;

public class LC268MissingNumber {
  class Solution1 {
    public int missingNumber(int[] nums) {
      int res = nums.length;
      for (int i = 0; i < nums.length; i++) {
        res ^= i ^ nums[i];
      }
      return res;
    }
  }

  class Solution2 {
    public int missingNumber(int[] nums) {
      int sum = (nums.length + 1) * nums.length / 2;
      int actualSum = 0;
      for (int num : nums) {
        actualSum += num;
      }
      return sum - actualSum;
    }
  }
}
