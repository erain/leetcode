package me.yiyu.leetcode.week5;

public class LC80RemoveDuplicatedFromSortedArrayII {
  class Solution {
    public int removeDuplicates(int[] nums) {
      int count = 1, i = 0;
      for (int j = 1; j < nums.length; j++) {
        if (nums[i] != nums[j]) {
          count = 1;
          nums[++i] = nums[j];
        } else {
          if (count < 2) {
            nums[++i] = nums[j];
          }
          count++;
        }
      }
      return i+1;
    }
  }
}
