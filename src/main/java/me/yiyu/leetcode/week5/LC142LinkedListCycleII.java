package me.yiyu.leetcode.week5;

import me.yiyu.leetcode.random.ListNode;

public class LC142LinkedListCycleII {
  public class Solution {
    public ListNode detectCycle(ListNode head) {
      if (head == null || head.next == null) return null;
      ListNode slow = head;
      ListNode fast = head;
      while (fast != null && fast.next != null) {
        slow = slow.next;
        fast = fast.next.next;
        if (fast == slow) {
          ListNode slow2 = head;
          while (slow!= slow2) {
            slow = slow.next;
            slow = slow2.next;
          }
          return slow;
        }
      }

      return null;
    }
  }
}
