package me.yiyu.leetcode.week5;

import java.util.Arrays;

public class LC287FindTheDuplicateNumber {
  class Solution1 {
    public int findDuplicate(int[] nums) {
      Arrays.sort(nums);
      // not OK, since the input array cannot be modified
      for (int i = 0; i < nums.length - 1; i++){
        if (nums[i] == nums[i + 1]) return nums[i];
      }
      return -1;
    }
  }

  class Solution2 {
    public int findDuplicate(int[] nums) {
      int low = 1, high = nums.length - 1, mid;
      while (low < high) {
        mid = low + (high - low) / 2;
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
          if (nums[i] <= mid)
            count++;
        }
        if (count > mid)
          high = mid;
        else
          low = mid + 1;
      }
      return low;
    }
  }

  class Solution3 {
    public int findDuplicate(int[] nums) {
      // The "tortoise and hare" step.  We start at the end of the array and try
      // to find an intersection point in the cycle.
      int slow = nums[0];
      int fast = nums[nums[0]];

      // Keep advancing 'slow' by one step and 'fast' by two steps until they
      // meet inside the loop.
      while (slow != fast)
      {
        slow = nums[slow];
        fast = nums[nums[fast]];
      }

      // Start up another pointer from the end of the array and march it forward
      // until it hits the pointer inside the array.
      int finder = 0;
      while (finder != slow)
      {
        finder = nums[finder];
        slow = nums[slow];
      }
      return slow;
    }
  }

  class Solution4 {
    // For the simpler version of the question:
    // what if there is only one element repeated twice.
    public int findDuplicate(int[] nums) {
      int sum = 0;
      for (int num : nums) sum += num;
      int targetSum = (1 + nums.length) * nums.length / 2;
      return nums.length - (targetSum - sum);
    }
  }

  class Solution5 {
    // not ok, we cannot modify the input nums
    public int findDuplicate(int[] nums) {
      for (int i = 0; i < nums.length; i++) {
        int index = Math.abs(nums[i]) - 1;
        if (nums[index] > 0) {
          nums[index] = -nums[index];
        } else {
          return index + 1;
        }
      }
      return -1;
    }
  }

}
