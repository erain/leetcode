package me.yiyu.leetcode.week8;

public class LC44WildcardMatching {
  class Solution1 {
    public boolean isMatch(String s, String p) {
      int sp = 0, pp = 0;
      int match = 0;
      int star = -1;
      while (sp < s.length()) {
        if (pp < p.length() &&
            (s.charAt(sp) == p.charAt(pp) || p.charAt(pp) == '?')) {
          sp++;
          pp++;
        } else if (pp < p.length() && p.charAt(pp) == '*') {
          star = pp;
          match = sp;
          pp++;
        } else if (star != -1) {
          pp = star + 1;
          match++;
          sp = match;
        } else return false;
      }

      while (pp < p.length() && p.charAt(pp) == '*') pp++;

      return pp == p.length();
    }
  }

  // https://www.youtube.com/watch?v=3ZDZ-N0EPV0
  class Solution2 {
    public boolean isMatch(String s, String p) {
      if(s==null || p==null) return false;
      int s_len = s.length();
      int p_len = p.length();

      boolean[][] d = new boolean[s_len+1][p_len+1];
      d[0][0] = true;

      for (int i = 0; i < p_len; i++) {
        if ( p.charAt(i) == '*') d[0][i+1] = d[0][i];
      }

      for (int i = 0; i < s_len; i++) {
        for (int j = 0; j < p_len; j++) {
          if(p.charAt(j) == '?') d[i+1][j+1] = d[i][j];
          if(p.charAt(j) == s.charAt(i)) d[i+1][j+1] = d[i][j];
          if(p.charAt(j) == '*') d[i+1][j+1] = d[i+1][j] || d[i][j+1];
        }
      }

      return d[s_len][p_len];
    }
  }
}
