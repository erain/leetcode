package me.yiyu.leetcode.week8;

public class LC45JumpGameII {

  class Solution {
    public int jump(int[] nums) {
      int curtStep = 0;
      int curtStepMaxReach = 0;
      int nextStepMaxReach = 0;
      for (int i = 0; i < nums.length - 1; i++) {
        nextStepMaxReach = Math.max(nextStepMaxReach, i + nums[i]);
        if (i == curtStepMaxReach) {
          curtStep++;
          curtStepMaxReach = nextStepMaxReach;
        }
      }
      return curtStep;
    }
  }

}
