package me.yiyu.leetcode.week8;

import java.util.Stack;

public class LC32LongestValidParentheses {

  class Solutio1 {
    public int longestValidParentheses(String s) {
      Stack<Integer> stack = new Stack<>();
      int res = 0;
      int start = -1;
      for (int i = 0; i < s.length(); i++) {
        if (s.charAt(i) == '(') {
          stack.push(i);
        } else {
          if (stack.isEmpty()) {
            start = i;
          } else {
            stack.pop();
            if (stack.isEmpty()) {
              res = Math.max(res, i - start);
            } else {
              res = Math.max(res, i - stack.peek());
            }
          }
        }
      }
      return res;
    }
  }


}
