package me.yiyu.leetcode.week8;

public class LC486PredictTheWinner {

  class Solution1 {
    public boolean PredictTheWinner(int[] nums) {
      return winner(nums, 0, nums.length - 1, 1) >= 0;
    }

    public int winner(int[] nums, int l, int r, int turn) {
      if (l == r) return turn * nums[l];
      int left = turn * nums[l] + winner(nums, l + 1, r, -turn);
      int right = turn * nums[r] + winner(nums, l, r - 1, -turn);
      return turn * Math.max(turn * left, turn * right);
    }
  }

  class Solution2 {
    public boolean PredictTheWinner(int[] nums) {
      return getScore(nums, 0, nums.length - 1) >= 0;
    }

    private int getScore(int[] nums, int l, int r) {
      if (l == r) return nums[l];
      int left = nums[l] - getScore(nums, l + 1, r);
      int right = nums[r] - getScore(nums, l, r - 1);
      return Math.max(left, right);
    }
  }

  class Solution3 {
    public boolean PredictTheWinner(int[] nums) {
      Integer[][] memo = new Integer[nums.length][nums.length];
      return getScore(nums, 0, nums.length - 1, memo) >= 0;
    }

    private int getScore(int[] nums, int l, int r, Integer[][] memo) {
      if (l == r) return nums[l];
      if (memo[l][r] != null) return memo[l][r];
      int left = nums[l] - getScore(nums, l + 1, r, memo);
      int right = nums[r] - getScore(nums, l, r - 1, memo);
      memo[l][r] = Math.max(left, right);
      return memo[l][r];
    }
  }

  class Solution4 {
    public boolean PredictTheWinner(int[] nums) {
      int[][] dp = new int[nums.length][nums.length];
      for (int i = 0; i < nums.length; i++) dp[i][i] = nums[i];
      for (int i = nums.length - 2; i >= 0; i--) {
        for (int j = i + 1; j < nums.length; j++) {
          int left = nums[i] - dp[i + 1][j];
          int right = nums[j] - dp[i][j - 1];
          dp[i][j] = Math.max(left, right);
        }
      }
      return dp[0][nums.length - 1] >= 0;
    }
  }
}
