package me.yiyu.leetcode.week8;

public class LC55JumpGame {

  public class Solution1 {

    public boolean dfs(int[] nums, int position) {
      if (position == nums.length - 1) {
        return true;
      }
      int furthestJump = Math.min(position + nums[position], nums.length - 1);
      for (int nextPosition = position + 1; nextPosition <= furthestJump; nextPosition++) {
        if (dfs(nums, nextPosition)) {
          return true;
        }
      }
      return false;
    }

    public boolean canJump(int[] nums) {
      return dfs(nums, 0);
    }
  }

  enum Index {
    GOOD, BAD, UNKNOWN
  }

  class Solution2 {

    Index[] memo;

    public boolean canJumpFromPosition(int position, int[] nums) {
      if (memo[position] != Index.UNKNOWN) {
        return memo[position] == Index.GOOD ? true : false;
      }

      int furthestJump = Math.min(position + nums[position], nums.length - 1);
      for (int nextPosition = furthestJump; nextPosition > position; nextPosition--) {
        if (canJumpFromPosition(nextPosition, nums)) {
          memo[position] = Index.GOOD;
          return true;
        }
      }

      memo[position] = Index.BAD;
      return false;
    }

    public boolean canJump(int[] nums) {
      memo = new Index[nums.length];
      for (int i = 0; i < memo.length; i++) {
        memo[i] = Index.UNKNOWN;
      }
      memo[memo.length - 1] = Index.GOOD;
      return canJumpFromPosition(0, nums);
    }
  }

  public class Solution3 {

    public boolean canJump(int[] nums) {
      Index[] memo = new Index[nums.length];
      for (int i = 0; i < memo.length; i++) {
        memo[i] = Index.UNKNOWN;
      }
      memo[memo.length - 1] = Index.GOOD;

      for (int i = nums.length - 2; i >= 0; i--) {
        int furthestJump = Math.min(i + nums[i], nums.length - 1);
        for (int j = i + 1; j <= furthestJump; j++) {
          if (memo[j] == Index.GOOD) {
            memo[i] = Index.GOOD;
            break;
          }
        }
      }

      return memo[0] == Index.GOOD;
    }
  }

  class Solution4 {
    public boolean canJump(int[] nums) {
      int lastPos = nums.length - 1;
      for (int i = nums.length - 1; i >= 0; i--) {
        if (i + nums[i] >= lastPos) {
          lastPos = i;
        }
      }
      return lastPos == 0;
    }
  }

  class Solution5 {
    public boolean canJump(int[] A) {
      int max = 0;
      for (int i = 0; i < A.length; i++) {
        if (i > max) {
          return false;
        }
        max = Math.max(A[i] + i, max);
      }
      return true;
    }
  }


}
